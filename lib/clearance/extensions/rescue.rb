if defined?(ActionController::Base)
  ActionDispatch::ExceptionWrapper.rescue_responses.update('ActionController::Forbidden' => :forbidden)
  # ActionDispatch::ShowExceptions.rescue_responses.update('ActionController::Forbidden' => :forbidden)
end
